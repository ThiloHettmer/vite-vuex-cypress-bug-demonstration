import { createApp } from 'vue'
import App from './App.vue'
import { createStore } from 'vuex'

// Create a new store instance.
const store = createStore({
  state () {
    return {
      count: 0
    }
  },
  mutations: {
    increment (state) {
      state.count++
    }
  }
})

const app = createApp(App).use(store).mount('#app')

//@ts-ignore
if (window.Cypress) {
  //@ts-ignore
  window.app = app
}
