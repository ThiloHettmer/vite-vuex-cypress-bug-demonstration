describe('Test vuex store', () => {
  const getStore = () => cy.window().its('app.$store')

  it('should have vuex stores', () => {
    cy.visit('/')
    getStore().its('state').should('exist')
  })
})